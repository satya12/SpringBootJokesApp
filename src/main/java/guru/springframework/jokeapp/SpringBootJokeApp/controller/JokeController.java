package guru.springframework.jokeapp.SpringBootJokeApp.controller;

import guru.springframework.jokeapp.SpringBootJokeApp.service.JokeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JokeController
{
   private JokeService jokeService;

   @Autowired
    public JokeController(JokeService jokeService)
    {
        this.jokeService = jokeService;
    }
    @RequestMapping("/")
    public String showJokes(Model model)
    {
      model.addAttribute("jokes",jokeService.getJokes());

      return "chucknorris";
    }

}
