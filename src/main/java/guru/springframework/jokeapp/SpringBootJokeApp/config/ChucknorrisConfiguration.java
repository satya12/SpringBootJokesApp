package guru.springframework.jokeapp.SpringBootJokeApp.config;

import guru.springframework.norris.chuck.ChuckNorrisQuotes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class ChucknorrisConfiguration
{
   @Bean
    public ChuckNorrisQuotes chuckNorrisQuotes()
   {
       return new ChuckNorrisQuotes();
   }
}
