package guru.springframework.jokeapp.SpringBootJokeApp.service;


import guru.springframework.norris.chuck.ChuckNorrisQuotes;
import org.springframework.stereotype.Service;

@Service
public class JokeServiceImpl implements JokeService
{

    private final ChuckNorrisQuotes chuckNorrisQuotes;
//
//    public JokeServiceImpl() {                        //this will be wused in chucknorissquote
//        this.chuckNorrisQuotes=new ChuckNorrisQuotes();
//    }


    public JokeServiceImpl(ChuckNorrisQuotes chuckNorrisQuotes) {
        this.chuckNorrisQuotes = chuckNorrisQuotes;
    }

    @Override
    public String getJokes() {
        return chuckNorrisQuotes.getRandomQuote();
    }
}
